package com.app.sort;

import java.util.Arrays;

public class SelectedSort {

    public static void main(String[] args) {
        int[] data = new int[]{1, 3, 60, 32, 1, 5, 89, 90, 101, 66, 43};
        System.out.println("before:" + Arrays.toString(data));
        SelectedSort ss = new SelectedSort();
        ss.select_sort(data);
        System.out.println("after:" + Arrays.toString(data));
    }


    public void select_sort(int[] data) {
        int index = 0;
        while (index < data.length) {
            int tmp = index;
            for (int i = index + 1; i < data.length; i++) {
                if (data[tmp] > data[i]) {
                    tmp = i;
                }
            }
            //交换index和tmp
            int value = data[index];
            data[index] = data[tmp];
            data[tmp] = value;
            index++;
        }
    }
}
