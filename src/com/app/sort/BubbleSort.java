package com.app.sort;

import java.util.Arrays;

public class BubbleSort {

    public static void main(String[] args) {
        BubbleSort bs = new BubbleSort();
        bs.bubble_sort(new int[]{1, 2, 56, 32, 78, 9, 0, 12, 5, 12});
    }


    public void bubble_sort(int[] data) {
        System.out.println("before sort:" + Arrays.toString(data));
        for (int i = 0; i < data.length - 1; i++) {
            for (int j = 0; j < data.length - 1 - i; j++) {
                if (data[j] > data[j + 1]) {
                    int tmp = data[j];
                    data[j] = data[j + 1];
                    data[j + 1] = tmp;
                }
            }
        }
        System.out.println("after sorted:" + Arrays.toString(data));
    }

}
